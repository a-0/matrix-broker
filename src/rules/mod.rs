use matrix_sdk::room::Room;
use serde::{Deserialize, Serialize};
use std::sync::Arc;
use tracing::debug;

use crate::db::DBHandle;


#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Rule {
    pub rule_id: String,
    pub pipeline: String,
    pub trigger_callbacks: Vec<String>,
    pub receiver_mxids: String, //Regular expression
    pub static_values: String,
}

pub async fn match_rules(
    db_handle: Arc<DBHandle>,
    cb_name: &str,
    receiver_mxid: &str,
    room: &Room,
    event_sender_mxid: &str,
) -> Vec<(Rule, String)> {
    let rule_candidates = db_handle.get_rule_candidates(cb_name, receiver_mxid).await;
    
    let mut pipeline_inputs: Vec<(Rule, String)> = vec![];

    for rule in rule_candidates {
        // Prevent loopback: ignore events sent by the receiver
        if event_sender_mxid == receiver_mxid {
            continue;
        }

        debug!("Rule matched: {:?}", rule);
        pipeline_inputs.push((rule.clone(), rule.static_values.clone()));
    }
    pipeline_inputs
}
