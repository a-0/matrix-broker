use serde::{Deserialize, Serialize};

pub mod features;

#[derive(Debug, Serialize, Deserialize)]
pub struct Instruction {
    sender_mxid: String,
    room_id: String,
    event_type: String,
    event_content: String,
}