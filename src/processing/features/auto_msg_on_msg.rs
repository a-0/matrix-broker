use std::sync::{Arc, RwLock};

use matrix_sdk::{
    room::Room,
    ruma::events::{
        SyncMessageLikeEvent, room::message::{MessageType, TextMessageEventContent, RoomMessageEventContent},
    },
};

use crate::matrix::{handler::MatrixHandler, send};

pub async fn on_room_message(
    matrix_handler: Arc<RwLock<MatrixHandler>>,
    parameters: &str,
    event: &SyncMessageLikeEvent<RoomMessageEventContent>,
    room: Room,
    rec_mxid: &str,
) {
    match &event.as_original().unwrap().content.msgtype {
        MessageType::Text(TextMessageEventContent { body: msg_body, .. }) => {
            if let Room::Joined(r) = room {
                r.send(RoomMessageEventContent::text_plain(&(String::from(parameters) + &msg_body)), None).await;
            }
        }
        _ => { /* message content could not be matched */ }
    }
}
