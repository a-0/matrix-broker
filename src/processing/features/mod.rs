pub mod auto_msg_on_msg;

pub fn get_features_list() -> Vec<&'static str> {
    vec!["auto_msg_on_msg"]
}
