use serde::{Serialize, Deserialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct AddRuleBody {
    pub pipeline_name: String,
    pub trigger_callbacks: Vec<String>,
    pub receiver_mxids: String,
    pub static_values: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct AddAccountBody {
    pub mxid: String,
    pub homeserver: String,
    pub password: String,
}