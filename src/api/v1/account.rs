use std::sync::{RwLock, Arc};

use actix_web::{HttpRequest, web::{self, Json}, post, HttpResponse};
use tracing::{error};

use crate::{matrix::User, matrix::handler::MatrixHandler, api::util::request_bodies::AddAccountBody };

#[post("/account/add")]
async fn add_account(_req: HttpRequest, mh: web::Data<Arc<RwLock<MatrixHandler>>>, json: Json<AddAccountBody>) -> HttpResponse {
    let mh_readguard = match mh.read() {
        Ok(rg) => rg,
        Err(e) => {
            error!("Failed to read MatrixHandler while processing request:\n{}", e);
            return HttpResponse::InternalServerError().body("Error while processing request");
        }
    };
    let u = User {
        id: json.mxid.clone(), 
        hs: json.homeserver.clone(), 
        is_virtual: false, 
        password: json.password.clone(), 
        device_id: "".to_owned(), 
        access_token: "".to_owned()
    };
    mh_readguard.db_handle.add_user(u).await;
    HttpResponse::Ok().body("")
}