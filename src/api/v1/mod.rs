use actix_web::{HttpRequest, get};


mod account;
pub use account::add_account;

mod rules;
pub use rules::add_rule;


#[get("/")]
async fn index(_req: HttpRequest) -> String {
    "Test output".to_owned()
}

