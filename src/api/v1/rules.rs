use std::sync::{RwLock, Arc};

use actix_web::{HttpRequest, web::{self, Json}, post, HttpResponse};
use tracing::{error};

use crate::{rules::Rule, matrix::handler::MatrixHandler, api::util::request_bodies::{ AddRuleBody }};

#[post("/rules/add")]
async fn add_rule(_req: HttpRequest, mh: web::Data<Arc<RwLock<MatrixHandler>>>, json: Json<AddRuleBody>) -> HttpResponse {
    // Prepare rule to be passed to the database handle
    let r = Rule {
        rule_id: "".to_owned(),
        pipeline: json.pipeline_name.clone(),
        trigger_callbacks: json.trigger_callbacks.clone(),
        receiver_mxids: json.receiver_mxids.clone(),
        static_values: json.static_values.clone(),
    };
    // Attempt to obtain a read guard on the global matrix handler
    let mh_readguard = match mh.read() {
        Ok(rg) => rg,
        Err(e) => {
            error!("Failed to read MatrixHandler while processing request:\n{}", e);
            return HttpResponse::InternalServerError().body("Error while processing request");
        }
    };
    // Add the rule to the database
    let result = mh_readguard.db_handle.add_rule(r).await;
    
    drop(mh_readguard);
    
    match result {
        Ok(rule_id) => HttpResponse::Ok().body(format!("{{ rule_id: {} }}", rule_id)),
        Err(error) => {
            error!("Error adding rule to database:\n{}", error);
            HttpResponse::InternalServerError().body("Error while processing request")
        }
    }
}