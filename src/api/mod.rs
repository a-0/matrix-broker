use std::sync::{Arc, RwLock};

use crate::{matrix::handler::MatrixHandler, config::SystemConfig};
use actix_rt::Arbiter;
use actix_server::Server;
use actix_web::{App, HttpServer, web, HttpResponse};
use tracing::{debug, error};

mod v1;
mod util;

async fn default_answer() -> HttpResponse {
    HttpResponse::Ok().body("{supported_api_versions: ['v1']}")
}

pub async fn run_listener(matrix_handler: Arc<RwLock<MatrixHandler>>, config: &SystemConfig) {
    let s = HttpServer::new(move || {
        let v1_service = web::scope("/v1").service(v1::index).service(v1::add_rule);

        App::new()
            // Pass context data to all handler functions
            .app_data(web::Data::new(matrix_handler.clone()))
            // Add version independent routes
            .route("/", web::get().to(default_answer))
            // Add sets of routes for each supported version
            .service(v1_service)
    })
    .bind((config.api.ip_addr.to_owned(), config.api.port)).expect(&format!("Failed to bind API listener to [{}]:{}", config.api.ip_addr, config.api.port))
    .run();

    if Arbiter::new().spawn(run(s)) {
        debug!("Started API server");
    }
    else {
        error!("Failed to spawn API server thread");
    }
}

async fn run(s: Server) {
    match s.await {
        Ok(_) => (),
        Err(e) => error!("Failed to run API server:\n{:?}", e),
    }
}