use matrix_sdk::{
    ruma::{
        events::{
            reaction::{ReactionEventContent, Relation},
            room::message::{TextMessageEventContent, RoomMessageEventContent},
        },
        EventId, RoomId,
    },
    Client,
};
use serde::{Deserialize, Serialize};
use std::sync::{Arc, RwLock};
use tracing::warn;

use crate::matrix::handler::MatrixHandler;

// Sends any kind of RoomMessage
/*
pub async fn room(
    sender_mxid: &str,
    room_id: &RoomId,
    event_type: &str,
    content: &str,
    mh_lock: Arc<RwLock<MatrixHandler>>,
) {
    let event_obj = match event_type {
        "m.text" => RoomMessageEventContent::text_plain(content),
        _ => panic!("")
    };
    let _response = match get_client(sender_mxid, mh_lock) {
        Some(c) => c
            .room_send(room_id, event_obj, None)
            .await
            .expect("Sending the message returned an error"),
        None => return,
    };
}*/

pub fn get_client(sender_mxid: &str, mh_lock: Arc<RwLock<MatrixHandler>>) -> Option<Arc<Client>> {
    let matrix_handler = mh_lock.read().expect("Failed to read MatrixHandler");
    match (*matrix_handler).clients.get(sender_mxid) {
        Some(c) => Some(c.clone()),
        None => None,
    }
}
/*
fn create_message_event_content(event_type: &str, content: &str) -> Option<MessageEventContent> {
    match event_type {
        "m.text" => Some(RoomMessageEventContent::text_plain(content)),
        "m.annotation" => {
            #[derive(Serialize, Deserialize)]
            struct ReactJsonStruct {
                content: String,
                event_id: EventId,
            }
            match serde_json::from_str(content) as Result<ReactJsonStruct, _> {
                Ok(c) => Some(AnyMessageEventContent::Reaction(ReactionEventContent::new(
                    Relation::new(c.event_id, c.content),
                ))),
                Err(_) => {
                    warn!("Received faulty reaction content from PipelineOutput. Pipeline may be broken");
                    return None;
                }
            }
        }
        _ => {
            //TODO attempt to send custom events
            warn!("Received faulty event_type from PipelineOutput. Pipeline may be broken");
            None
        }
    }
}
*/