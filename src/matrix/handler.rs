use matrix_sdk::{
    ruma::{events::room::message::RoomMessageEventContent},
    Session,
};
use std::{
    collections::HashMap,
    sync::{Arc, RwLock},
};
use tracing::{debug, info, warn};
use url::Url;

use matrix_sdk::{
    config::SyncSettings, room::Room, ruma::events::SyncMessageLikeEvent, ruma::UserId, Client,
    LoopCtrl,
};

// TODO: integrate with server.rs for event handling, exchange and proccesing
use crate::{config::SystemConfig, db::DBHandle, matrix::event_callbacks, matrix::User};

pub struct MatrixHandler {
    //users: Vec<matrix::User>,
    pub cfg: Arc<SystemConfig>,
    pub clients: HashMap<String, Arc<Client>>, //<mxid, Client>
    pub db_handle: Arc<DBHandle>,
    pub sync_active: bool,
}

#[derive(Clone)]
pub struct EventHandlingContext {
    pub mxid: String,
    pub db_handle: Arc<DBHandle>,
    pub matrix_handler: Arc<RwLock<MatrixHandler>>,
}

impl MatrixHandler {
    pub async fn new(cfg: Arc<SystemConfig>, db_handle: Arc<DBHandle>) -> Arc<RwLock<Self>> {
        let mh = Arc::new(RwLock::new(MatrixHandler {
            cfg: cfg.clone(),
            clients: HashMap::new(),
            db_handle: db_handle.clone(),
            sync_active: true,
        }));

        let mut clients: HashMap<String, Arc<Client>> = HashMap::new();
        // Inefficient, parallelization may be helpful
        let users = db_handle.get_all_users().await;
        debug!("Read {} users from database.", users.len());
        for u in &users {
            match launch_matrix_account(
                u,
                db_handle.clone(),
                cfg.client_storage_path.clone(),
                mh.clone(),
            )
            .await
            {
                Some(c) => clients.insert(String::from(&u.id), c),
                None => continue,
            };
        }

        let writable_mh = mh.write();
        match writable_mh {
            Ok(mut handler) => handler.clients = clients,
            Err(_) => {
                panic!("Failed to create matrix handler");
            }
        }
        debug!("Matrix Handler created.");
        mh.clone()
    }
    pub async fn cleanup(&self) {}
}

pub async fn start_client_sync(c: Arc<Client>, mh: Arc<RwLock<MatrixHandler>>) {
    let res = c.sync_with_callback(SyncSettings::new(), |_| {
        let cloned = mh.clone();
        async move {
            if match cloned.clone().read() {
                Ok(mh_rg) => mh_rg.sync_active,
                Err(e) => {
                    warn!("Unable to read MatrixHandler to acquire SYNC_ACTIVE, retrying to sync. Error: {}", e);
                    true
                }
            } {
                LoopCtrl::Continue
            } else {
                LoopCtrl::Break
            }
        }
    }).await;
    match res {
        Ok(_) => (),
        Err(e) => {
            let mxid = c
                .user_id()
                .expect("Failed to extract mxid from Client object");
            warn!("[{}] Failed to sync client:\n{}", mxid, e)
        }
    }
}

pub async fn launch_matrix_account(
    user: &User,
    db_handle: Arc<DBHandle>,
    storage_path: String,
    matrix_handler: Arc<RwLock<MatrixHandler>>,
) -> Option<Arc<Client>> {
    debug!("[{}] Attempting to launch matrix client", &user.id);
    let ruma_uid = match UserId::parse(&user.id.clone()) {
        Ok(id) => id,
        Err(_) => {
            warn!("[{}] Invalid Matrix ID!", &user.id);
            return None;
        }
    };

    let hs_url = Url::parse(&user.hs).expect("Failed to parse hs url from client storage");
    let acc_storage_path: String =
        storage_path + ruma_uid.server_name().as_str() + "/" + ruma_uid.localpart() + "/";
    let mut no_store_found = false;
    if !std::path::Path::new(&acc_storage_path).is_dir() {
        match std::fs::create_dir_all(&acc_storage_path) {
            Ok(_) => (),
            Err(e) => {
                warn!("[{}] Failed to create directory for client store:\n{}", &user.id, e)
            }
        }
        no_store_found = true;
        info!(
            "[{}] No store found, dev_id: {}, len: {}",
            &user.id,
            &user.device_id,
            &user.device_id.len()
        );
    }
    let client_builder = Client::builder().homeserver_url(hs_url);
    let client_builder_with_store = match client_builder.sled_store(&acc_storage_path, None) {
        Ok(store) => store,
        Err(e) => {
            warn!(
                "[{}] Failed to open store with path \"{}\":\n{}",
                &user.id, &acc_storage_path, e
            );
            return None;
        }
    };
    let client = match client_builder_with_store.build().await {
        Ok(c) => c,
        Err(e) => {
            warn!("[{}] Failed to build matrix client:\n{}", &user.id, e);
            return None;
        }
    };

    let event_handler_context = EventHandlingContext {
        mxid: user.id.clone(),
        db_handle: db_handle.clone(),
        matrix_handler: matrix_handler.clone(),
    };
    add_event_handlers(&client, event_handler_context).await;

    let client_arc = Arc::new(client);

    // Perform fresh login
    if user.device_id.len() == 0 || no_store_found {
        debug!("[{}] Logging in using mxid and password", &user.id);
        match client_arc
            .login_username(&user.id, &user.password)
            .send()
            .await
        {
            Ok(response) => {
                db_handle
                    .set_client_session(
                        &user.id,
                        response.device_id.as_str(),
                        response.access_token.as_str(),
                    )
                    .await
            }
            Err(e) => {
                warn!("[{}] Error logging in:\n{:?}", &user.id, e);
                return None;
            },
        }
    }
    // Restore previous session
    else {
        debug!("[{}] Logging in using stored session", &user.id);
        let session = Session {
            access_token: user.access_token.clone(),
            device_id: user.device_id.clone().into(),
            user_id: ruma_uid,
            refresh_token: None,
        };
        match client_arc.restore_login(session).await {
            Ok(_) => (),
            Err(e) => {
                warn!("[{}] Error restoring previous session:\n{:?}", &user.id, e);
                return None;
            },
        }
    }

    debug!("[{}] Spawning sync thread", &user.id);
    tokio::spawn(start_client_sync(
        client_arc.clone(),
        matrix_handler.clone(),
    ));
    debug!("[{}] Successfully created client", &user.id);
    Some(client_arc)
}

async fn add_event_handlers(client: &Client, ctx: EventHandlingContext) {
    client.add_event_handler(
        move |ev: SyncMessageLikeEvent<RoomMessageEventContent>, room: Room| {
            let context = ctx.clone();
            async move {
                event_callbacks::on_room_message(ev, room, context).await;
            }
        },
    );
}

/*
async fn on_room_member(event: &SyncStateEvent<MemberEventContent>, room: Room) {
    pipeline::execute(
        self.db_handle.clone(),
        &self.ints.clone(),
        "on_room_member",
        &self.mxid,
        event,
        room,
        self.matrix_handler.clone(),
    )
    .await;
}
async fn on_room_name(event: &SyncStateEvent<NameEventContent>, room: Room) {
    pipeline::execute(
        self.db_handle.clone(),
        &self.ints.clone(),
        "on_room_name",
        &self.mxid,
        event,
        room,
        self.matrix_handler.clone(),
    )
    .await;
}
async fn on_room_canonical_alias(

    room: Room,
    event: &SyncStateEvent<CanonicalAliasEventContent>, room: Room
) {
    pipeline::execute(
        self.db_handle.clone(),
        &self.ints.clone(),
        "on_room_canonical_alias",
        &self.mxid,
        event,
        room,
        self.matrix_handler.clone(),
    )
    .await;
}
async fn on_room_aliases(event: &SyncStateEvent<AliasesEventContent>, room: Room) {
    pipeline::execute(
        self.db_handle.clone(),
        &self.ints.clone(),
        "on_room_aliases",
        &self.mxid,
        event,
        room,
        self.matrix_handler.clone(),
    )
    .await;
}
async fn on_room_avatar(event: &SyncStateEvent<AvatarEventContent>, room: Room) {
    pipeline::execute(
        self.db_handle.clone(),
        &self.ints.clone(),
        "on_room_avatar",
        &self.mxid,
        event,
        room,
        self.matrix_handler.clone(),
    )
    .await;
}
async fn on_room_message(event: &SyncMessageEvent<MessageEventContent>, room: Room) {
    pipeline::execute(
        self.db_handle.clone(),
        &self.ints.clone(),
        "on_room_message",
        &self.mxid,
        event,
        room,
        self.matrix_handler.clone(),
    )
    .await;
}
async fn on_room_message_feedback(
    room: Room,
    event: &SyncMessageEvent<FeedbackEventContent>, room: Room,
) {
    pipeline::execute(
        self.db_handle.clone(),
        &self.ints.clone(),
        "on_room_message_feedback",
        &self.mxid,
        event,
        room,
        self.matrix_handler.clone(),
    )
    .await;
}
async fn on_room_call_invite(event: &SyncMessageEvent<InviteEventContent>, room: Room) {
    pipeline::execute(
        self.db_handle.clone(),
        &self.ints.clone(),
        "on_room_call_invite",
        &self.mxid,
        event,
        room,
        self.matrix_handler.clone(),
    )
    .await;
}
async fn on_room_call_answer(event: &SyncMessageEvent<AnswerEventContent>, room: Room) {
    pipeline::execute(
        self.db_handle.clone(),
        &self.ints.clone(),
        "on_room_call_answer",
        &self.mxid,
        event,
        room,
        self.matrix_handler.clone(),
    )
    .await;
}
async fn on_room_call_candidates(

    room: Room,
    event: &SyncMessageEvent<CandidatesEventContent>, room: Room,
) {
    pipeline::execute(
        self.db_handle.clone(),
        &self.ints.clone(),
        "on_room_call_candidates",
        &self.mxid,
        event,
        room,
        self.matrix_handler.clone(),
    )
    .await;
}
async fn on_room_call_hangup(event: &SyncMessageEvent<HangupEventContent>, room: Room) {
    pipeline::execute(
        self.db_handle.clone(),
        &self.ints.clone(),
        "on_room_call_hangup",
        &self.mxid,
        event,
        room,
        self.matrix_handler.clone(),
    )
    .await;
}
async fn on_room_redaction(event: &SyncRedactionEvent, room: Room) {
    pipeline::execute(
        self.db_handle.clone(),
        &self.ints.clone(),
        "on_room_redaction",
        &self.mxid,
        event,
        room,
        self.matrix_handler.clone(),
    )
    .await;
}
async fn on_room_power_levels(event: &SyncStateEvent<PowerLevelsEventContent>, room: Room) {
    pipeline::execute(
        self.db_handle.clone(),
        &self.ints.clone(),
        "on_room_power_levels",
        &self.mxid,
        event,
        room,
        self.matrix_handler.clone(),
    )
    .await;
}
async fn on_room_join_rules(event: &SyncStateEvent<JoinRulesEventContent>, room: Room) {
    pipeline::execute(
        self.db_handle.clone(),
        &self.ints.clone(),
        "on_room_join_rules",
        &self.mxid,
        event,
        room,
        self.matrix_handler.clone(),
    )
    .await;
}
async fn on_room_tombstone(event: &SyncStateEvent<TombstoneEventContent>, room: Room) {
    pipeline::execute(
        self.db_handle.clone(),
        &self.ints.clone(),
        "on_room_tombstone",
        &self.mxid,
        event,
        room,
        self.matrix_handler.clone(),
    )
    .await;
}
async fn on_state_member(event: &SyncStateEvent<MemberEventContent>, room: Room) {
    pipeline::execute(
        self.db_handle.clone(),
        &self.ints.clone(),
        "on_state_member",
        &self.mxid,
        event,
        room,
        self.matrix_handler.clone(),
    )
    .await;
}
async fn on_state_name(event: &SyncStateEvent<NameEventContent>, room: Room) {
    pipeline::execute(
        self.db_handle.clone(),
        &self.ints.clone(),
        "on_state_name",
        &self.mxid,
        event,
        room,
        self.matrix_handler.clone(),
    )
    .await;
}
async fn on_state_canonical_alias(

    room: Room,
    event: &SyncStateEvent<CanonicalAliasEventContent>, room: Room,
) {
    pipeline::execute(
        self.db_handle.clone(),
        &self.ints.clone(),
        "on_state_canonical_alias",
        &self.mxid,
        event,
        room,
        self.matrix_handler.clone(),
    )
    .await;
}
async fn on_state_aliases(event: &SyncStateEvent<AliasesEventContent>, room: Room) {
    pipeline::execute(
        self.db_handle.clone(),
        &self.ints.clone(),
        "on_state_aliases",
        &self.mxid,
        event,
        room,
        self.matrix_handler.clone(),
    )
    .await;
}
async fn on_state_avatar(event: &SyncStateEvent<AvatarEventContent>, room: Room) {
    pipeline::execute(
        self.db_handle.clone(),
        &self.ints.clone(),
        "on_state_avatar",
        &self.mxid,
        event,
        room,
        self.matrix_handler.clone(),
    )
    .await;
}
async fn on_state_power_levels(event: &SyncStateEvent<PowerLevelsEventContent>, room: Room) {
    pipeline::execute(
        self.db_handle.clone(),
        &self.ints.clone(),
        "on_state_power_levels",
        &self.mxid,
        event,
        room,
        self.matrix_handler.clone(),
    )
    .await;
}
async fn on_state_join_rules(event: &SyncStateEvent<JoinRulesEventContent>, room: Room) {
    pipeline::execute(
        self.db_handle.clone(),
        &self.ints.clone(),
        "on_state_join_rules",
        &self.mxid,
        event,
        room,
        self.matrix_handler.clone(),
    )
    .await;
}
// async fn on_custom_event( room_state: Room, event: &CustomEvent<'_>) {
//    pipeline::execute(&self.rule_set, "on_custom_event", &self.mxid, event);
// }
async fn on_stripped_state_aliases(

    room: Room,
    event: &StrippedStateEvent<AliasesEventContent>, room: Room
) {
    pipeline::execute(
        self.db_handle.clone(),
        &self.ints.clone(),
        "on_stripped_state_aliases",
        &self.mxid,
        event,
        room,
        self.matrix_handler.clone(),
    )
    .await;
}
async fn on_stripped_state_avatar(

    room: Room,
    event: &StrippedStateEvent<AvatarEventContent>, room: Room
) {
    pipeline::execute(
        self.db_handle.clone(),
        &self.ints.clone(),
        "on_stripped_state_avatar",
        &self.mxid,
        event,
        room,
        self.matrix_handler.clone(),
    )
    .await;
}
async fn on_stripped_state_canonical_alias(

    room: Room,
    event: &StrippedStateEvent<CanonicalAliasEventContent>, room: Room
) {
    pipeline::execute(
        self.db_handle.clone(),
        &self.ints.clone(),
        "on_stripped_state_canonical_alias",
        &self.mxid,
        event,
        room,
        self.matrix_handler.clone(),
    )
    .await;
}
async fn on_stripped_state_join_rules(

    room: Room,
    event: &StrippedStateEvent<JoinRulesEventContent>, room: Room
) {
    pipeline::execute(
        self.db_handle.clone(),
        &self.ints.clone(),
        "on_stripped_state_join_rules",
        &self.mxid,
        event,
        room,
        self.matrix_handler.clone(),
    )
    .await;
}
async fn on_stripped_state_member(

    room: Room,
    event: &StrippedStateEvent<MemberEventContent>, room: Room
    _: Option<MemberEventContent>,
) {
    pipeline::execute(
        self.db_handle.clone(),
        &self.ints.clone(),
        "on_stripped_state_member",
        &self.mxid,
        event,
        room,
        self.matrix_handler.clone(),
    )
    .await;
}
async fn on_stripped_state_name(event: &StrippedStateEvent<NameEventContent>, room: Room) {
    pipeline::execute(
        self.db_handle.clone(),
        &self.ints.clone(),
        "on_stripped_state_name",
        &self.mxid,
        event,
        room,
        self.matrix_handler.clone(),
    )
    .await;
}
async fn on_stripped_state_power_levels(

    room: Room,
    event: &StrippedStateEvent<PowerLevelsEventContent>, room: Room
) {
    pipeline::execute(
        self.db_handle.clone(),
        &self.ints.clone(),
        "on_stripped_state_power_levels",
        &self.mxid,
        event,
        room,
        self.matrix_handler.clone(),
    )
    .await;
}
*/
