#![feature(str_split_once)]
use std::convert::Infallible;
use std::net::SocketAddr;
use hyper::{Body, Request, Response, Server, StatusCode, Method, body::{aggregate, Buf, to_bytes}};
use hyper::service::{make_service_fn, service_fn};
use serde::{ Serialize, Deserialize };
use serde_json::json;
use serde_qs;
use mut_static::MutStatic;
use lazy_static::{self};
use regex::Regex;
use urlencoding::{encode, decode};

use crate::{
    logging::{log,debug},
    matrix::registration,
};

lazy_static::lazy_static!{
    static ref RE: MutStatic<Vec<Regex>> = MutStatic::new();
    static ref HS_TOKEN: MutStatic<Token> = MutStatic::new();
}

#[derive(Debug, Serialize, Deserialize, Clone)]
struct Token {
    access_token: String,
}

// TODO: proper error handling
async fn handle(_req: Request<Body>) -> Result<Response<Body>, Infallible> {
    debug("incomming request");
    let (parts, body) = _req.into_parts(); 
    debug(&format!("{:#?}", parts));
    //debug(&format!("{:#?}", body));

    // get access_token from query string
    let query_str = String::from(parts.uri.query().unwrap());   
    let query: Token = serde_qs::from_str(&query_str).unwrap();
    debug(&format!("{:#?}",query)); 

    // verify access_token
    if query.access_token != HS_TOKEN.read().unwrap().access_token {
        let mut err = Response::new(Body::empty());
        *err.body_mut() = Body::from(json!({
                                    "errcode":"COM.EXAMPLE.MYAPPSERVICE_UNAUTHORIZED"
                            }).to_string());
        *err.status_mut() = StatusCode::UNAUTHORIZED;
        Ok(err)
    } else {
        let res: Vec<Regex> = RE.read().unwrap().to_vec();
        
        // strip uri for better parsing/matching
        let mut uri = parts.uri.path().strip_prefix("/_matrix/app/v1");
        if uri.is_none() {
            uri = parts.uri.path().strip_prefix("/_matrix/app/unstable");
            if uri.is_none() {
                uri = Some(parts.uri.path());
            }
        }

        // TODO: switch to split_once, when it's stable
        //let (_path,_uri) = uri.unwrap()[1..].split_once('/').unwrap();
        let _uri_:Vec<&str> = uri.unwrap()[1..].splitn(2, '/').collect();
        let _path = _uri_[0];
        let _uri = _uri_[1];

        let mut response = Response::new(Body::empty());
        match _path {
            "users" => {
                let uid = decode(_uri).unwrap();
                
                if (res.iter().map(|re| re.is_match(&uid))).fold(false, |acc, exist| (acc | exist)) {
                    *response.status_mut() = StatusCode::OK;
                    debug("user ok");
                } else {
                    debug(&format!("namespace {:?} not found", uid));
                    *response.status_mut() = StatusCode::NOT_FOUND;
                    *response.body_mut() = Body::from(json!({"errcode": "COM.EXAMPLE.MYAPPSERVICE_NOT_FOUND"}).to_string());
                }
            },
            "transactions" => {
                let txid = _uri;
                *response.status_mut() = StatusCode::OK;
                debug("transaction ok");
                if parts.headers.contains_key("content-type")  && parts.headers["content-type"] == "application/json" {
                    let _body = aggregate(body).await.unwrap();
                    let bod_jso : serde_json::Value = serde_json::from_reader(_body.reader()).unwrap();
                    debug(&format!("{:#?}", bod_jso));

                    // TODO: further process/handle events
                    // TODO: use ruma / matrix-sdk predefined event structs
                }

            },
            "rooms" => {
                debug("rooms not implemented");
                let rid = _uri;
                // TODO: implement room query
            },
            "thirdparty" => {
                debug("thirdparty not implemented");
                // let (_path,_uri) = _uri.split_once('/').unwrap();
                // TODO: implement thirdparty stuff
            },
            _ => {
                *response.status_mut() = StatusCode::NOT_FOUND;
            },
        };

        Ok(response)
    }
}


// sample code from docs:
pub async fn start(reg: registration::Registration) {
    // Construct our SocketAddr to listen on...
    let addr: SocketAddr = (reg.url.strip_prefix("http://").unwrap()).parse().expect("Unable to parse address");


    HS_TOKEN.set(Token { access_token: String::from(&reg.hs_token), });    
    let regex: Vec<Regex> = reg.namespaces.users.iter().map(|usr| Regex::new(&usr.regex).unwrap()).collect();
    RE.set(regex);

    // And a MakeService to handle each connection...
    let make_service = make_service_fn(|_conn| async {
        Ok::<_, Infallible>(service_fn(handle))
    });

    // Then bind and serve...
    let server = Server::bind(&addr).serve(make_service);

    // And run forever...
    if let Err(e) = server.await {
        eprintln!("server error: {}", e);
    }
}
