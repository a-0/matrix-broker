use tracing::{info, warn};
use matrix_sdk::{room::Room, };
use crate::{matrix::handler::{EventHandlingContext}, rules, processing::features};
use matrix_sdk::ruma::events::{SyncMessageLikeEvent, room::message::RoomMessageEventContent};

pub async fn on_room_message(
    event: SyncMessageLikeEvent<RoomMessageEventContent>,
    room: Room,
    context: EventHandlingContext,
) {
    info!("Received room message.");
    let pipeline_inputs = rules::match_rules(
        context.db_handle.clone(),
        "on_room_message",
        &context.mxid,
        &room,
        event.sender().as_str(),
    )
    .await;
    
    for (rule, parameters) in pipeline_inputs {
        match rule.pipeline.as_str() {
            "auto_msg_on_msg" => {
                features::auto_msg_on_msg::on_room_message(
                    context.matrix_handler.clone(),
                    &parameters,
                    &event,
                    room.clone(),
                    &context.mxid,
                )
                .await
            }
            _ => warn!("Pipeline name could not be matched: {}", rule.pipeline),
        }
    }
}