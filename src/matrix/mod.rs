use serde::{Deserialize, Serialize};

pub mod handler;
pub mod send;
pub mod event_callbacks;

#[derive(Serialize, Deserialize, Clone)]
pub struct User {
    pub id: String,
    pub hs: String,
    pub is_virtual: bool,
    pub password: String,
    pub device_id: String,
    pub access_token: String,
}
