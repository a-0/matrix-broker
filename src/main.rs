use std::env;
use std::sync::atomic::AtomicBool;
use std::sync::atomic::Ordering;
use std::sync::Arc;
use std::thread::sleep;
use std::time::Duration;
use tracing::warn;

use tracing::{debug, info};
use tracing_subscriber::prelude::*;
use tracing_subscriber::{
    filter::{self, LevelFilter},
    fmt,
};

use crate::db::*;

mod api;
mod config;
mod db;
mod matrix;
mod processing;
mod rules;

// arguments: main(cfg_path: &str)
#[actix_web::main]
async fn main() {
    // Set up logging
    let (writer, _guard) = tracing_appender::non_blocking(std::io::stdout());
    let filter = filter::Targets::new()
        .with_target("matrix_broker", tracing::Level::TRACE)
        .with_target("matrix_sdk_base", tracing::Level::WARN)
        .with_target("hyper", LevelFilter::OFF)
        .with_target("sled", tracing::Level::WARN)
        .with_target("actix_server", tracing::Level::INFO)
        .with_target("sqlx", tracing::Level::WARN)
        .with_target("", tracing::Level::INFO);

    tracing_subscriber::registry()
        .with(fmt::layer().with_writer(writer).json())
        .with(filter)
        .init();

    // Parse arguments
    let args: Vec<String> = env::args().collect();
    debug!("args passed: {:?}", args);

    // System setup
    let cfg: Arc<config::SystemConfig> = Arc::new(config::SystemConfig::new(&args[1]));
    debug!("config loaded:");
    debug!("{:#?}", cfg);

    // Initialize DBHandle
    let dbh: Arc<DBHandle> = Arc::new(DBHandle::new(&cfg.db_path).await);

    let mh = matrix::handler::MatrixHandler::new(cfg.clone(), dbh.clone()).await;

    debug!("matrix_handler successfully created");

    // Start listening for API calls
    api::run_listener(mh.clone(), &cfg).await;

    // Catch termination signals
    let termination = Arc::new(AtomicBool::new(false));
    match signal_hook::flag::register(signal_hook::consts::SIGTERM, termination.clone()) {
        Ok(_) => (),
        Err(_) => {
            warn!("Could not register SIGTERM as termination signal")
        }
    };
    match signal_hook::flag::register(signal_hook::consts::SIGINT, termination.clone()) {
        Ok(_) => (),
        Err(_) => {
            warn!("Could not register SIGINT as termination signal")
        }
    };

    while !termination.load(Ordering::Relaxed) {
        sleep(Duration::from_millis(100));
    }

    info!("Received termination signal, starting cleanup");

    match mh.write() {
        Ok(mut mh_wg) => mh_wg.sync_active = false,
        Err(e) => {
            warn!("Could not deactivate sync_active, pipelines may be interrupted unexpectedly during execution. Error: {}", e)
        }
    }

    // Cleanup & shutdown procedures
    mh.read().expect("").cleanup().await;
}
