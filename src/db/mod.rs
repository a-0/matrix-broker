use crate::{matrix::User, rules::Rule};
use sqlx::{PgPool, SqlitePool};
use std::{fs};
use tracing::{error, warn};

mod queries;
mod types;

static MODE_UNINIT: i64 = 0;
static MODE_SQLITE: i64 = 1;
static MODE_POSTGRES: i64 = 2;

pub struct DBHandle {
    mode: i64,
    sqlite_pool: Option<SqlitePool>,
    postgres_pool: Option<PgPool>,
}

impl DBHandle {
    pub async fn new(path: &str) -> DBHandle {
        let mut handle = DBHandle {
            mode: MODE_UNINIT,
            sqlite_pool: None,
            postgres_pool: None,
        };
        if String::from(path).starts_with("sqlite://") {
            handle.sqlite_pool = Some(create_sqlite_handle(path).await);
            handle.mode = MODE_SQLITE;
        } else {
            error!("Unsupported or malformed db path!");
            std::process::exit(1);
        }
        handle
    }

    pub async fn from_file(&self, path: &str) {}
    pub async fn to_file(&self, path: &str) {}

    pub async fn add_rule(&self, r: Rule) -> Result<String, String>{
        if self.mode == MODE_SQLITE {
            queries::sqlite_add_rule(
                match &self.sqlite_pool {
                    Some(p) => p,
                    _ => {
                        return Err("Couldn't match database pool".to_string());
                    }
                },
                r,
            )
            .await
        } else {
            error!("DBHandle in erroneous mode: {}, killing...", self.mode);
            std::process::exit(1);
        }
    }
    pub async fn add_user(&self, u: User) {
        if self.mode == MODE_SQLITE {
            queries::sqlite_add_user(
                match &self.sqlite_pool {
                    Some(p) => p,
                    _ => {
                        return;
                    }
                },
                u,
            )
            .await
        } else {
            error!("DBHandle in erroneous mode: {}, killing...", self.mode);
            std::process::exit(1);
        }
    }
    pub async fn set_client_session(&self, mxid: &str, device_id: &str, access_token: &str) {
        if self.mode == MODE_SQLITE {
            queries::sqlite_set_client_session(
                match &self.sqlite_pool {
                    Some(p) => p,
                    _ => {
                        return;
                    }
                },
                mxid,
                device_id,
                access_token,
            )
            .await
        } else {
            error!("DBHandle in erroneous mode: {}, killing...", self.mode);
            std::process::exit(1);
        }
    }

    pub async fn get_rule_candidates(&self, callback: &str, receiver: &str) -> Vec<Rule> {
        if self.mode == MODE_SQLITE {
            queries::sqlite_get_rule_candidates(
                match &self.sqlite_pool {
                    Some(p) => p,
                    _ => {
                        return vec![];
                    }
                },
                callback,
                receiver,
            )
            .await
        } else {
            error!("DBHandle in erroneous mode: {}, killing...", self.mode);
            std::process::exit(1);
        }
    }
    pub async fn lookup_user(&self, mxid: &str) -> Option<User> {
        if self.mode == MODE_SQLITE {
            queries::sqlite_lookup_user(
                match &self.sqlite_pool {
                    Some(p) => p,
                    _ => {
                        return None;
                    }
                },
                mxid,
            )
            .await
        } else {
            error!("DBHandle in erroneous mode: {}, killing...", self.mode);
            std::process::exit(1);
        }
    }
    pub async fn get_all_users(&self) -> Vec<User> {
        if self.mode == MODE_SQLITE {
            queries::sqlite_get_all_users(match &self.sqlite_pool {
                Some(p) => p,
                _ => {
                    return vec![];
                }
            })
            .await
        } else {
            error!("DBHandle in erroneous mode: {}, killing...", self.mode);
            std::process::exit(1);
        }
    }
}

pub async fn create_sqlite_handle(path: &str) -> SqlitePool {
    let mut init_user: Option<User> = None;

    if !std::path::Path::new(&path[9..]).exists() {
        warn!("SQLite database file not found! Creating new file...");
        if let Err(e) = fs::File::create(&path[9..]) {
            error!("Failed to create SQLite database file: {}", e);
            std::process::exit(1);
        };
        //TODO: Better setup process. Temporary dialog to add first user
        let mut mxid = String::new();
        let mut hs = String::new();
        let mut pw = String::new();
        let mut dev_id = String::new();
        println!("No user has been added to the database yet. Creating first user\nmxid: ");
        std::io::stdin().read_line(&mut mxid).unwrap();
        println!("homeserver url: ");
        std::io::stdin().read_line(&mut hs).unwrap();
        println!("password: ");
        std::io::stdin().read_line(&mut pw).unwrap();
        println!("device id (leave empty if none has been used with this matrix-broker before): ");
        std::io::stdin().read_line(&mut dev_id).unwrap();
        init_user = Some(User {
            device_id: dev_id.replace("\n", ""),
            hs: hs.replace("\n", ""),
            id: mxid.replace("\n", ""),
            password: pw.replace("\n", ""),
            access_token: "".to_owned(),
            is_virtual: false,
        });
    }
    let pool = match SqlitePool::connect(path).await {
        Ok(p) => p,
        Err(e) => {
            error!("Failed to connect to SQLite database: {}", e);
            std::process::exit(1);
        }
    };

    // If schema is non-existent, create it
    if true {
        queries::sqlite_try_create_schema(&pool).await;
    }

    match init_user {
        Some(u) => queries::sqlite_add_user(&pool, u).await,
        None => (),
    }

    pool
}
