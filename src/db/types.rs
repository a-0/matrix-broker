#[derive(Clone)]
pub struct User {
    pub mxid: Option<String>,
    pub hs_url: Option<String>,
    pub password: Option<String>,
    pub device_id: Option<String>,
    pub access_token: Option<String>,
}
pub struct Rule {
    pub rule_id: Option<String>,
    pub pipeline: Option<String>,
    pub receiver_mxids: Option<String>,
    pub static_values: Option<String>,
}

impl User {
    pub fn to_native(&self) -> Option<crate::matrix::User> {
        let mxid = match &self.mxid {
            Some(s) => String::from(s),
            _ => {
                return None;
            }
        };
        let hs = match &self.hs_url {
            Some(s) => String::from(s),
            _ => {
                return None;
            }
        };
        let pw = match &self.password {
            Some(s) => String::from(s),
            _ => {
                return None;
            }
        };
        let device_id = match &self.device_id {
            Some(s) => String::from(s),
            _ => String::from("")
        };
        let access_token = match &self.access_token {
            Some(s) => String::from(s),
            _ => String::from("")
        };


        Some(crate::matrix::User {
            id: mxid,
            hs,
            is_virtual: pw.is_empty(),
            device_id,
            access_token,
            password: pw,
        })
    }
}
impl Rule {
    pub fn to_native(&self) -> Option<crate::rules::Rule> {
        let rule_id = match &self.rule_id {
            Some(s) => String::from(s),
            _ => {
                return None;
            }
        };
        let pipeline = match &self.pipeline {
            Some(s) => String::from(s),
            _ => {
                return None;
            }
        };
        let receiver_mxids = match &self.receiver_mxids {
            Some(s) => String::from(s),
            _ => {
                return None;
            }
        };
        let static_values = match &self.static_values {
            Some(s) => String::from(s),
            _ => {
                return None;
            }
        };

        Some(crate::rules::Rule {
            rule_id,
            pipeline,
            trigger_callbacks: vec![],
            receiver_mxids,
            static_values,
        })
    }
}
