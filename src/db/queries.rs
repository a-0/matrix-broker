use rand::{distributions::Alphanumeric, thread_rng, Rng};
use sqlx::SqlitePool;
use tracing::{error, warn};

use crate::{matrix::User, rules::Rule};

pub async fn sqlite_try_create_schema(pool: &SqlitePool) {
    match sqlx::query!(
        "
    CREATE TABLE IF NOT EXISTS rules(
        rule_id TEXT PRIMARY KEY,
        pipeline TEXT,
        receiver_mxids TEXT,
        static_values TEXT
    );
    CREATE TABLE IF NOT EXISTS callback_map(
        callback TEXT,
        rule_id TEXT
    );
    CREATE TABLE IF NOT EXISTS users(
        mxid TEXT PRIMARY KEY,
        hs_url TEXT,
        password TEXT,
        device_id TEXT,
        access_token TEXT
    );
    ",
    )
    .execute(pool)
    .await
    {
        Ok(_) => (),
        Err(e) => {
            error!("{}", e);
        }
    }
}

pub async fn sqlite_add_rule(pool: &SqlitePool, r: Rule) -> Result<String, String>{
    let rule_id: String = thread_rng()
        .sample_iter(&Alphanumeric)
        .take(32)
        .map(char::from)
        .collect();

    match sqlx::query!(
        "
    INSERT INTO rules(rule_id, pipeline, receiver_mxids, static_values)
    VALUES (?, ?, ?, ?);
    ",
        rule_id,
        r.pipeline,
        r.receiver_mxids,
        r.static_values
    )
    .execute(pool)
    .await
    {
        Ok(_) => (),
        Err(e) => {
            warn!("{}", e);
        }
    }
    for cb in r.trigger_callbacks {
        match sqlx::query!(
            "
        INSERT INTO callback_map
        VALUES (?, ?);
        ",
            cb,
            rule_id
        )
        .execute(pool)
        .await
        {
            Ok(_) => (),
            Err(e) => {
                warn!("{}", e);
            }
        };
    }
    Ok(rule_id)
}

pub async fn sqlite_add_user(pool: &SqlitePool, u: User) {
    match sqlx::query!(
        "
    INSERT INTO users(mxid, hs_url, password, device_id, access_token)
    VALUES (?, ?, ?, ?, ?);
    ",
        u.id,
        u.hs,
        u.password,
        u.device_id,
        u.access_token
    )
    .execute(pool)
    .await
    {
        Ok(_) => (),
        Err(e) => {
            warn!("{}", e);
        }
    }
}
pub async fn sqlite_set_client_session(pool: &SqlitePool, mxid: &str, device_id: &str, access_token: &str) {
    match sqlx::query!(
        "
    UPDATE users
    SET device_id = ?, access_token = ?
    WHERE mxid = ?
    ",
        device_id,
        access_token,
        mxid
    )
    .execute(pool)
    .await
    {
        Ok(_) => (),
        Err(e) => {
            warn!("{}", e);
        }
    }
}

pub async fn sqlite_lookup_user(pool: &SqlitePool, mxid: &str) -> Option<User> {
    match sqlx::query_as!(
        crate::db::types::User,
        "
    SELECT mxid, hs_url, password, device_id, access_token
    FROM users
    WHERE mxid=?
    ",
        mxid
    )
    .fetch_one(pool)
    .await
    {
        Ok(u) => u.to_native(),
        _ => None,
    }
}
pub async fn sqlite_get_all_users(pool: &SqlitePool) -> Vec<User> {
    let mut users = vec![];
    match sqlx::query_as!(
        crate::db::types::User,
        "
    SELECT mxid, hs_url, password, device_id, access_token
    FROM users
    "
    )
    .fetch_all(pool)
    .await
    {
        Ok(u) => {
            for db_user in u {
                if let Some(usr) = db_user.to_native() {
                    users.push(usr);
                }
            }
        }
        _ => (),
    };
    users
}

pub async fn sqlite_get_rule_candidates(
    pool: &SqlitePool,
    cb: &str,
    recv: &str,
) -> Vec<crate::rules::Rule> {
    let mut rules_post_recv: Vec<crate::rules::Rule> = vec![];

    match sqlx::query_as!(
        crate::db::types::Rule,
        "
    SELECT rule_id, pipeline, receiver_mxids, static_values
    FROM rules
    WHERE rule_id IN (
        SELECT rule_id
        FROM callback_map
        WHERE callback = ?
    ) AND receiver_mxids = ?
    ",
        cb,
        recv
    )
    .fetch_all(pool)
    .await
    {
        Ok(rules) => {
            for rule in rules {
                if let Some(r) = rule.to_native() {
                    rules_post_recv.push(r);
                }
            }
        }
        _ => (),
    };
    rules_post_recv
}
