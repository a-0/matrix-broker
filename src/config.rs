use serde::{Deserialize, Serialize};
use std::fs;

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct ApiConfig {
    pub ip_addr: String,
    pub port: u16,
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct SystemConfig {
    pub matrix_hs_address: String,
    pub as_address: String,
    pub as_port: String,
    pub matrix_as_secret: String,
    pub matrix_as_namespaces: Vec<String>,
    pub as_name: String,
    pub as_local_part: String,
    pub rule_sources: Vec<String>,
    pub db_path: String,
    pub client_storage_path: String,
    pub api: ApiConfig,
}

impl SystemConfig {
    pub fn new(path: &str) -> SystemConfig {
        let yaml_str = fs::read_to_string(path).expect("Error while reading config file");
        let cfg: SystemConfig =
            serde_yaml::from_str(&yaml_str).expect("Config file not in expected format");
        cfg
    }
}
