FROM docker.io/library/rust:latest

RUN mkdir /var/matrix-broker
WORKDIR /var/matrix-broker

ADD ./src ./src
COPY ./Cargo.toml .
COPY ./config.yaml .
COPY ./.env .
COPY ./test.db .

CMD cargo run