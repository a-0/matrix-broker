Commits are barely guaranteed to build...

# Build
Requires:
- Installation of openssl, openssl-devel and cmake packages

# Documentation
The documentation can be found in docs/.

It can also be built as html using mdBook. Instructions & information can be found here: https://github.com/rust-lang/mdBook