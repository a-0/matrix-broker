# matrix-broker

This is the documentation of the matrix-broker application, a "soon to be" powerful matrix-to-matrix bridge.

This application is in the early development phase, **we do not recommend to use it with any non-temporary matrix account!**

For feature requests, bug reports etc., please have a look at the issues of the git repository (Link...).

## Goal & Scope of this Application
The aim is to provide an easy way to automate processing of matrix events within the ecosystem (as compared to bots or "real" bridges, which may also communicate with non-matrix systems). 

Matrix-broker will enable **functionalities like mailing-list equivalents**, **alias accounts** which can be published without revealing one's private account or may be passed to the successor in office, **and much much more**.

It is our goal to make matrix ready to be used in organisations by enabling features which made e-mail flourish.