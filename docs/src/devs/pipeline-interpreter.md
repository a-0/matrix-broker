*(this is only a sketch)*

# Function Descriptions

## init()
Most PipelineInterpreters will need to do some initialization work, e.g. load files of pipeline scripts. No externally visible effect is expected from this function.

Calling this function again later should not leave the interpreter in an inconsistent state!

**return** Ok(()) if no notable errors occurred, Err(String) else. Helpful error messages are appreciated.

## get_pipeline_list()
Errors may be logged, in case they affect the execution of pipelines, the affected must not appear in the return value.

**return** A Vec\<String> containing all names of pipelines ready to be called.

## call_pipeline(...)
Actual execution of a pipeline. The name parameter should be matched case sensitively, and must be recognized if it was contained in the result of a getPipelineList() call.

**return** Ok(String) in case of success - the String must be in valid YAML/JSON/?? syntax. Err(String) is returned in case of error - helpful error messages are appreciated.
