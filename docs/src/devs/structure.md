# Important Components
## Pipeline
Pipelines are abstract instructions on how to process a matrix event. They define *how* an event shall be handled, but not the parameters.

**Example:** A "forward" pipeline, which describes how the forwarded message can be constructed from the input message, and what parameters it needs to funcion, like the alias name messages are sent to from others, and the actual receiver address messages will be forwarded to.

Pipelines may be coded in Rust directly (faster), or in a script language (more flexible) as long as the necessary interpreter is implemented in matrix-broker. All pipelines are accessed through their corresponding PipelineInterpreter implementation.

As Pipelines are Turing-complete and, depending on the implementation of the interpreter, may have significant access to the host system, they are expected to be added by developers and motivated admins, *not* by users.

## Rules
Rules contain the necessary parameters to instantiate a pipeline.

**Example:** Sticking to the pipeline example, the rule would contain data like both mentioned matrix IDs, but depending on how sophisticated the pipeline is (and what parameters it can use) may also contain spam filtering conditions, maximum message lengths or styling of the forwarded message.

Rules are expected to be added by anyone using the service actively, that is by users. Role Based Access Control etc. should exist to mitigate abuse of the system.

## PipelineInterpreters
PipelineInterpreters provide a unified interface for the matrix-broker code, regardless of the pipeline's "format" (hardcoded Rust pipelines, scripts, binaries). They provide all necessary functions, first and foremost a function taking the matrix event, the matched rule and optional parameters and returning either a list of proposed matrix events to be sent, or an error.

# Event Flow
Note: This is the concept, not all steps and features mentioned here are already implemented.
1. Matrix events fetched, either through push- or sync-api
2. Events are matched with configured rules
3. Every found match is then checked for validity (e.g. whether a rule may use a specific account, ...)
4. The pipeline specified in the rule is called, the event, the rule content and (if needed) additional arguments are passed to the pipeline
5. The output of the pipeline is parsed by the matrix-broker, security checks are applied (e.g. too many receipients, no access to a requested room, loop detection, ...)
6. All permitted matrix events are created and sent
