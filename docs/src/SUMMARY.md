# Summary

- [Introduction](./intro.md)
- [For Users](./users.md)
- [For Admins](./admins.md)
- [For Developers](./devs.md)
  - [Structural Overview](./devs/structure.md)
  - [Pipeline Interpreters](./devs/pipeline-interpreter.md)
  - [Appservice](./devs/appservice.md)
  - [Database](./devs/db.md)